<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

$api = app('Dingo\Api\Routing\Router');

/*
| API Routes que requieren autenticación
|--------------------------------------------------------------------------
|
*/
$api->version('v1', ['middleware' => 'api.auth'], function ($api) {


/*
| CRUD USUARIOS
|--------------------------------------------------------------------------
|
*/

    $api->get('users', 'App\Http\Controllers\UsersController@index');
    $api->get('users/{id}', 'App\Http\Controllers\UsersController@show');
    $api->post('users', 'App\Http\Controllers\UsersController@store');
    $api->put('users/{id}', 'App\Http\Controllers\UsersController@update');
    $api->delete('users/{id}','App\Http\Controllers\UsersController@destroy');

/*
| CRUD USUARIOS ROLES
|--------------------------------------------------------------------------
|
*/

    $api->get('userRoles', 'App\Http\Controllers\UserRolController@index');
    $api->get('userRoles/{id}', 'App\Http\Controllers\UserRolController@show');
    $api->post('userRoles', 'App\Http\Controllers\UserRolController@store');
    $api->put('userRoles/{id}', 'App\Http\Controllers\UserRolController@update');
    $api->delete('userRoles/{id}','App\Http\Controllers\UserRolController@destroy');


/*
| CRUD USUARIOS ENTES
|--------------------------------------------------------------------------
|
*/

    $api->get('userEntes', 'App\Http\Controllers\UserEnteController@index');
    $api->get('userEntes/{id}', 'App\Http\Controllers\UserEnteController@show');
    $api->post('userEntes', 'App\Http\Controllers\UserEnteController@store');
    $api->put('userEntes/{id}', 'App\Http\Controllers\UserEnteController@update');
    $api->delete('userEntes/{id}','App\Http\Controllers\UserEnteController@destroy');


/*
| CRUD ENTES
|--------------------------------------------------------------------------
|
*/

    $api->get('entes', 'App\Http\Controllers\EnteController@index');
    $api->get('entes/{id}', 'App\Http\Controllers\EnteController@show');
    $api->post('entes', 'App\Http\Controllers\EnteController@store');
    $api->put('entes/{id}', 'App\Http\Controllers\EnteController@update');
    $api->delete('entes/{id}','App\Http\Controllers\EnteController@destroy');
    $api->get('entes/user/{id}','App\Http\Controllers\EnteController@enteByUser');

/*
| CRUD DERECHOHABIENTE
|--------------------------------------------------------------------------
|
*/

    $api->get('derechohabientes', 'App\Http\Controllers\DerechohabienteController@index');
    $api->get('derechohabientes/{id}', 'App\Http\Controllers\DerechohabienteController@show');
    $api->post('derechohabientes', 'App\Http\Controllers\DerechohabienteController@store');
    $api->put('derechohabientes/{id}', 'App\Http\Controllers\DerechohabienteController@update');
    $api->delete('derechohabientes/{id}','App\Http\Controllers\DerechohabienteController@destroy');

    // LISTA DE BENEFICIARIOS
    $api->get('derechohabientes/{id}/beneficiarios', 'App\Http\Controllers\DerechohabienteController@beneficiarios');

/*
| CRUD GRUPO SANGUINEO
|--------------------------------------------------------------------------
|
*/

    $api->get('grupoSanguineos', 'App\Http\Controllers\GrupoSanguineoController@index');
    $api->get('grupoSanguineos/{id}', 'App\Http\Controllers\GrupoSanguineoController@show');
    $api->post('grupoSanguineos', 'App\Http\Controllers\GrupoSanguineoController@store');
    $api->put('grupoSanguineos/{id}', 'App\Http\Controllers\GrupoSanguineoController@update');
    $api->delete('grupoSanguineo/{id}','App\Http\Controllers\GrupoSanguineoController@destroy');

/*
| CRUD ESTADO CIVIL
|--------------------------------------------------------------------------
|
*/

    $api->get('estadosCiviles', 'App\Http\Controllers\EstadoCivilController@index');
    $api->get('estadosCiviles/{id}', 'App\Http\Controllers\EstadoCivilController@show');
    $api->post('estadosCiviles', 'App\Http\Controllers\EstadoCivilController@store');
    $api->put('estadosCiviles/{id}', 'App\Http\Controllers\EstadoCivilController@update');
    $api->delete('estadosCiviles/{id}','App\Http\Controllers\EstadoCivilController@destroy');

/*
| CRUD DERECHOHABIENTE TIPO
|--------------------------------------------------------------------------
|
*/

    $api->get('derechohabienteTipos', 'App\Http\Controllers\DerechohabienteTipoController@index');
    $api->get('derechohabienteTipos/{id}', 'App\Http\Controllers\DerechohabienteTipoController@show');
    $api->post('derechohabienteTipos', 'App\Http\Controllers\DerechohabienteTipoController@store');
    $api->put('derechohabienteTipos/{id}', 'App\Http\Controllers\DerechohabienteTipoController@update');
    $api->delete('derechohabienteTipos/{id}','App\Http\Controllers\DerechohabienteTipoController@destroy');

/*
| CRUD MUNICIPIO
|--------------------------------------------------------------------------
|
*/

    $api->get('municipios', 'App\Http\Controllers\MunicipioController@index');
    $api->get('municipios/{id}', 'App\Http\Controllers\MunicipioController@show'); 

    // LISTAR CODIGOS POSTALES POR MUNICIPIOS
    $api->get('municipios/{id}/codigospostales','App\Http\Controllers\MunicipioController@codigospostales');



/*
|  ESTADO
|--------------------------------------------------------------------------
|
*/

    $api->get('estados', 'App\Http\Controllers\EstadoController@index');
    $api->get('estados/{id}', 'App\Http\Controllers\EstadoController@show');

    // LISTAR MUNICIPIOS POR ESTADO
    $api->get('estados/{idEstado}/municipios','App\Http\Controllers\EstadoController@municipios');



/*
| CRUD CODIGO POSTAL
|--------------------------------------------------------------------------
|
*/
    $api->get('codigospostales/{id}', 'App\Http\Controllers\CodigoPostalController@show'); 

    // LISTAR ASENTAMIENTOS POR CODIGO POSTAL
    $api->get('codigospostales/{id}/asentamientos','App\Http\Controllers\CodigoPostalController@asentamientos');


/*
| ASENTAMIENTO
|--------------------------------------------------------------------------
|
*/
    $api->get('asentamientos/{id}', 'App\Http\Controllers\AsentamientoController@show'); 



/*
|  VIALIDAD
|--------------------------------------------------------------------------
|
*/

    $api->get('vialidades', 'App\Http\Controllers\VialidadController@index');
    $api->get('vialidades/{id}', 'App\Http\Controllers\VialidadController@show') ;
    $api->post('vialidades', 'App\Http\Controllers\VialidadController@store');
    $api->put('vialidades/{id}', 'App\Http\Controllers\VialidadController@update');
    $api->delete('vialidades/{id}','App\Http\Controllers\VialidadController@destroy');


/*
| CRUD DOMICILIO
|--------------------------------------------------------------------------
|
*/

    $api->get('domicilios', 'App\Http\Controllers\DomicilioController@index');
    $api->get('domicilios/{id}', 'App\Http\Controllers\DomicilioController@show');
    $api->post('domicilios', 'App\Http\Controllers\DomicilioController@store');
    $api->put('domicilios/{id}', 'App\Http\Controllers\DomicilioController@update');
    $api->delete('domicilios/{id}','App\Http\Controllers\DomicilioController@destroy');

/*
| CRUD BENEFICIARIO
|--------------------------------------------------------------------------
|
*/

    $api->get('beneficiarios', 'App\Http\Controllers\BeneficiarioController@index');
    $api->get('beneficiarios/{id}', 'App\Http\Controllers\BeneficiarioController@show');
    $api->post('beneficiarios', 'App\Http\Controllers\BeneficiarioController@store');
    $api->put('beneficiarios/{id}', 'App\Http\Controllers\BeneficiarioController@update');
    $api->delete('beneficiarios/{id}','App\Http\Controllers\BeneficiarioController@destroy');


/*
| CRUD PARENTESCO
|--------------------------------------------------------------------------
|
*/

    $api->get('parentescos', 'App\Http\Controllers\BeneficiarioParentescoController@index');
    $api->get('parentescos/{id}', 'App\Http\Controllers\BeneficiarioParentescoController@show');
    $api->post('parentescos', 'App\Http\Controllers\BeneficiarioParentescoController@store');
    $api->put('parentescos/{id}', 'App\Http\Controllers\BeneficiarioParentescoController@update');
    $api->delete('parentescos/{id}','App\Http\Controllers\BeneficiarioParentescoController@destroy');


/*
| Rutas de autenticación
|--------------------------------------------------------------------------
|
*/
    $api->get('refresh', 'App\Http\Controllers\AuthenticateController@getToken');
    $api->get('logout', 'App\Http\Controllers\AuthenticateController@logout');
    $api->get('authenticatedUser', 'App\Http\Controllers\AuthenticateController@authenticatedUser');

});


/*
| Ruta login
|--------------------------------------------------------------------------
|
*/
$api->version('v1', function ($api) {    


    $api->post('login','App\Http\Controllers\AuthenticateController@authenticate');


    });
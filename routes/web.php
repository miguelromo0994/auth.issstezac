<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::group(['middleware' => ['auth.jwt:web','role:super-admin|editor|moderador']], function() {
    Route::get('/example', 'App\Http\Controllers\ExampleController@index')->name('example.index');
    Route::get('/example/create', 'App\Http\Controllers\ExampleController@create')->name('example.create');
    Route::get('/example/show', 'App\Http\Controllers\ExampleController@show')->name('example.show');
    });
<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Models\Beneficiario;
use League\Fractal\TransformerAbstract;

class BeneficiarioTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(Beneficiario $beneficiario)
    {

        return [
                'id'        => (int) $beneficiario->id,
                'estatus'   => $beneficiario->estatus,
                'rfc'       => $beneficiario->rfc, 
                'curp'      => $beneficiario->curp, 
                'nombre'    => $beneficiario->nombre, 
                'primerApellido'  => $beneficiario->primerApellido, 
                'segundoApellido' => $beneficiario->segundoApellido, 
                'fechaNac'  => $beneficiario->fechaNac, 
                'sexo'      => $beneficiario->sexo,
                'telefono'  => $beneficiario->telefono, 
                'esDerechohabiente' => $beneficiario->esDerechohabiente, 
                'observaciones'     => $beneficiario->observaciones, 
                'idGrupoSanguineo'  => $beneficiario->idGrupoSanguineo, 
                'idDerechohabiente' => $beneficiario->idDerechohabiente, 
                'idParentesco'      => $beneficiario->idParentesco, 
                'idEstadoCivil'     => $beneficiario->idEstadoCivil, 
                'idDomicilio'       => $beneficiario->idDomicilio,
        ];
    }
}
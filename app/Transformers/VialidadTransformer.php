<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Models\Vialidad;
use League\Fractal\TransformerAbstract;

class VialidadTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(Vialidad $vialidad)
    {

        return [
                'id'       => (int) $vialidad->id,
                'vialidad'   => $vialidad->vialidad, 
               
        ];
    }
}
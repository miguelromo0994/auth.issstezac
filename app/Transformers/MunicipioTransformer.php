<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Models\Municipio;
use League\Fractal\TransformerAbstract;

class MunicipioTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(Municipio $municipio)
    {

        return [
                'id'        => (int) $municipio->id,
                'nombre'   => $municipio->nombre, 
          		'codigo'   => $municipio->codigo, 
                'idEstado' => $municipio->idEstado, 
                       
        ];
    }
}
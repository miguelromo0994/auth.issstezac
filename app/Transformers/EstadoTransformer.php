<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Models\Estado;
use League\Fractal\TransformerAbstract;

class EstadoTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(Estado $estado)
    {

        return [
                'id'       => (int) $estado->id,
                'nombre'   => $estado->nombre, 
                'codigo'   => $estado->codigo  
        ];
    }
}
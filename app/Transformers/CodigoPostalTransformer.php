<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Models\CodigoPostal;
use League\Fractal\TransformerAbstract;

class CodigoPostalTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(CodigoPostal $codigo)
    {

        return [
                'id'       => (int) $codigo->id,
                'codigo'   => $codigo->codigo, 
                'idMunicipio'   => $codigo->idMunicipio  
        ];
    }
}
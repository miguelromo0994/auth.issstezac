<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Transformers\EnteTransformer;
use App\Models\EntePublico;
use League\Fractal\TransformerAbstract;

class EnteTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(EntePublico $ente)
    {

        return [
            'id'        => (int) $ente->id,
            'nombre'    => $ente->nombre, 
            'rfcPatronal' => $ente->rfcPatronal, 
            'telefono'    => $ente->telefono, 
            'clasificacion' => $ente->clasificacion, 
            'fechaIncorporacion' => $ente->fechaIncorporacion, 
            'fechaDesincorporacion' => $ente->fechaDesincorporacion, 
            'porcentajeCuota'       => $ente->porcentajeCuota, 
            'porcentajeAportacion'  => $ente->porcentajeAportacion, 
            'claveRegistroPatronal' => $ente->claveRegistroPatronal, 
            'idRegimenIncorporacion' => $ente->idRegimenIncorporacion, 
            'idEstatusEnte'         => $ente->idEstatusEnte, 
            'idRepresentanteLegal'  => $ente->idRepresentanteLegal,
            'idTabulador'       => $ente->idTabulador,
            'idDomicilio'       => $ente->idDomicilio,
            'idCalendarioCobros'=> $ente->idCalendarioCobros,
            'idEnteConcepto'    => $ente->idEnteConcepto,
        ];
    }
}
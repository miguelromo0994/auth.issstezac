<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Models\Asentamiento;
use League\Fractal\TransformerAbstract;

class AsentamientoTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(Asentamiento $asentamiento)
    {

        return [
                'id'       => (int) $asentamiento->id,
                'nombre'   => $asentamiento->nombre, 
                'tipo'     => $asentamiento->tipo,
				'ciudad'       => $asentamiento->ciudad,
				'zona'         => $asentamiento->zona,
                'idAsenta'     => $asentamiento->idAsenta,
                'idCodigoPostal' => $asentamiento->idCodigoPostal,
               
        ];
    }
}
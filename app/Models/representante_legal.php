<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idRepresentanteLegal
 * @property string $nombreRepresentanteStr
 * @property string $primerApellidoRepresentanteStr
 * @property string $segundoApellidoRepresentanteStr
 * @property string $rfcRepresentanteStr
 * @property string $curpRepresentanteStr
 * @property string $cargoRepresentanteStr
 * @property string $estatusRepresentanteStr
 * @property string $fechaInicioRepresentanteDte
 * @property string $fechaFinRepresentanteDte
 * @property int $idEnteLng
 * @property int $notificacion_idNotificacionLng
 * @property DocumentoDigitalizado $documentoDigitalizado
 * @property Notificacion $notificacion
 * @property EntePublico[] $entePublicos
 */
class representante_legal extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'representante_legal';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idRepresentanteLegal';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['nombreRepresentanteStr', 'primerApellidoRepresentanteStr', 'segundoApellidoRepresentanteStr', 'rfcRepresentanteStr', 'curpRepresentanteStr', 'cargoRepresentanteStr', 'estatusRepresentanteStr', 'fechaInicioRepresentanteDte', 'fechaFinRepresentanteDte', 'idEnteLng', 'notificacion_idNotificacionLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function documentoDigitalizado()
    {
        return $this->belongsTo('App\Models\documento_digitalizado', 'idRepresentanteLegal', 'idRelacionadoLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notificacion()
    {
        return $this->belongsTo('App\Models\notificacion', 'notificacion_idNotificacionLng', 'idNotificacionLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entePublicos()
    {
        return $this->hasMany('App\Models\ente_publico', 'representante_legal_idRepresentanteLegal', 'idRepresentanteLegal');
    }
}

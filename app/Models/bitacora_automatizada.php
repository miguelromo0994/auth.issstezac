<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 */
class bitacora_automatizada extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'bitacora_automatizada';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [];

}

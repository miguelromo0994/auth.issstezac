<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $estatus
 * @property string $rfc
 * @property string $curp
 * @property string $nss
 * @property string $nombre
 * @property string $primerApellido
 * @property string $segundoApellido
 * @property string $sexo
 * @property string $fechaNac
 * @property string $fechaDefuncion
 * @property string $fechaIngreso
 * @property string $estatusBloqueo
 * @property string $telefono
 * @property string $celular
 * @property string $email
 * @property string $emailAlterno
 * @property string $numeroIssstezac
 * @property int $idEstadoCivil
 * @property int $idDerechohabienteTipo
 * @property int $idGrupoSanguineo
 * @property int $idDomicilio

 */
class Derechohabiente extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'derechohabiente';

    /**
     * @var array
     */
    protected $fillable = ['id', 
                            'estatus', 
                            'rfc', 
                            'curp', 
                            'nss', 
                            'nombre', 
                            'primerApellido', 
                            'segundoApellido', 
                            'sexo', 
                            'fechaNac', 
                            'fechaDefuncion', 
                            'fechaIngreso', 
                            'estatusBloqueo', 
                            'telefono', 
                            'celular', 
                            'email', 
                            'emailAlterno', 
                            'numeroIssstezac', 
                            'idEstadoCivil',
                            'idDerechohabienteTipo', 
                            'idGrupoSanguineo', 
                            'idDomicilio'
                        ];

    public $timestamps = false;

}

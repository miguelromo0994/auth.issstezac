<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $idUnidadFilialLng
 * @property string $nombreStr
 * @property int $asignacion_sucursales_idAsignacionSucursalLng
 * @property int $idTipoSucursalLng
 * @property string $c_sucursalcol
 * @property integer $domicilio_idDomicilioLng
 * @property AsignacionSucursale $asignacionSucursale
 * @property Domicilio $domicilio
 * @property TipoSucursal[] $tipoSucursals
 */
class sucursal extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'sucursal';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idUnidadFilialLng';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nombreStr', 'asignacion_sucursales_idAsignacionSucursalLng', 'idTipoSucursalLng', 'c_sucursalcol', 'domicilio_idDomicilioLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function asignacionSucursale()
    {
        return $this->belongsTo('App\Models\asignacion_sucursales', 'asignacion_sucursales_idAsignacionSucursalLng', 'idAsignacionSucursalLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function domicilio()
    {
        return $this->belongsTo('App\Models\domicilio', 'domicilio_idDomicilioLng', 'idDomicilioLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tipoSucursals()
    {
        return $this->hasMany('App\Models\tipo_sucursal', 'c_sucursal_idUnidadFilialLng', 'idUnidadFilialLng');
    }
}

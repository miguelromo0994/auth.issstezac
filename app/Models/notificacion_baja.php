<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idNotificacionBajaLng
 * @property int $idNotificacionLng
 * @property int $idTipoBaja
 * @property int $idMotivoBaja
 * @property int $observacionesBajaStr
 * @property MotivoBaja[] $motivoBajas
 * @property Notificacion[] $notificacions
 * @property TipoBaja[] $tipoBajas
 */
class notificacion_baja extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'notificacion_baja';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idNotificacionBajaLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['idNotificacionLng', 'idTipoBaja', 'idMotivoBaja', 'observacionesBajaStr'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function motivoBajas()
    {
        return $this->hasMany('App\Models\motivo_baja', 'notificacion_baja_idNotificacionBajaLng', 'idNotificacionBajaLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notificacions()
    {
        return $this->hasMany('App\Models\notificacion', 'notificacion_baja_idNotificacionBajaLng', 'idNotificacionBajaLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tipoBajas()
    {
        return $this->hasMany('App\Models\tipo_baja', 'notificacion_baja_idNotificacionBajaLng', 'idNotificacionBajaLng');
    }
}

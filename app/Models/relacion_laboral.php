<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idRelacionLaboralLng
 * @property string $estatusStr
 * @property string $numeroEmpleadoStr
 * @property string $fechaIngresoDte
 * @property string $fechaBajaDefinitivaDte
 * @property string $puestoStr
 * @property int $credencialVigenteBit
 * @property string $credencialStr
 * @property int $periodicidadInt
 * @property float $sueldoQuincenalDbl
 * @property float $sueldoBaseCotizacionDbl
 * @property float $liquidezDbl
 * @property float $liquidezApartadaDbl
 * @property string $clabeStr
 * @property string $cuentaBancariaStr
 * @property int $idDerechohabienteLng
 * @property int $idSectorLng
 * @property int $idEnteLng
 * @property int $idMunicipioLng
 * @property int $idTipoRegimenLng
 * @property int $idLeyLng
 * @property int $idSindicatoLng
 * @property int $idBancoLng
 * @property int $idCentroCostosLng
 * @property int $idTabuladorLng
 * @property int $periodo_cotizacion_idPeriodoCotizacionLng
 * @property int $notificacion_idNotificacionLng
 * @property int $movimientos_cobros_idMovimientoCobroLng
 * @property Banco $banco
 * @property Municipio $municipio
 * @property EntePublico $entePublico
 * @property MovimientosCobro $movimientosCobro
 * @property Notificacion $notificacion
 * @property PeriodoCotizacion $periodoCotizacion
 * @property Sector $sector
 * @property CuotaAportacion[] $cuotaAportacions
 * @property Derechohabiente $derechohabiente
 * @property Tabulador[] $tabuladors
 */
class relacion_laboral extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'relacion_laboral';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idRelacionLaboralLng';

    /**
     * @var array
     */
    protected $fillable = ['estatusStr', 'numeroEmpleadoStr', 'fechaIngresoDte', 'fechaBajaDefinitivaDte', 'puestoStr', 'credencialVigenteBit', 'credencialStr', 'periodicidadInt', 'sueldoQuincenalDbl', 'sueldoBaseCotizacionDbl', 'liquidezDbl', 'liquidezApartadaDbl', 'clabeStr', 'cuentaBancariaStr', 'idDerechohabienteLng', 'idSectorLng', 'idEnteLng', 'idMunicipioLng', 'idTipoRegimenLng', 'idLeyLng', 'idSindicatoLng', 'idBancoLng', 'idCentroCostosLng', 'idTabuladorLng', 'periodo_cotizacion_idPeriodoCotizacionLng', 'notificacion_idNotificacionLng', 'movimientos_cobros_idMovimientoCobroLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function banco()
    {
        return $this->belongsTo('App\Models\banco', 'idBancoLng', 'idBancoLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function municipio()
    {
        return $this->belongsTo('App\Models\municipio', 'idMunicipioLng', 'idMunicipioLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entePublico()
    {
        return $this->belongsTo('App\Models\ente_publico', 'idEnteLng', 'idEnteLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function movimientosCobro()
    {
        return $this->belongsTo('App\Models\movimientos_cobros', 'movimientos_cobros_idMovimientoCobroLng', 'idMovimientoCobroLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notificacion()
    {
        return $this->belongsTo('App\Models\notificacion', 'notificacion_idNotificacionLng', 'idNotificacionLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function periodoCotizacion()
    {
        return $this->belongsTo('App\Models\periodo_cotizacion', 'periodo_cotizacion_idPeriodoCotizacionLng', 'idPeriodoCotizacionLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sector()
    {
        return $this->belongsTo('App\sector', 'idSectorLng', 'idSectorLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cuotaAportacions()
    {
        return $this->hasMany('App\Models\cuota_aportacion', 'relacion_laboral_idRelacionLaboralLng', 'idRelacionLaboralLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function derechohabiente()
    {
        return $this->hasOne('App\Models\derechohabiente', 'idDerechohabienteLng', 'idDerechohabienteLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tabuladors()
    {
        return $this->hasMany('App\Models\tabulador', 'relacion_laboral_idRelacionLaboralLng', 'idRelacionLaboralLng');
    }
}

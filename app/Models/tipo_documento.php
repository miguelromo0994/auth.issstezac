<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idTipoDocumentoLng
 * @property string $categoriaDocumentoStr
 * @property string $nombreDocumentoStr
 * @property int $documento_digitalizado_idDocumentoLng
 * @property DocumentoDigitalizado $documentoDigitalizado
 */
class tipo_documento extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tipo_documento';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idTipoDocumentoLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['categoriaDocumentoStr', 'nombreDocumentoStr', 'documento_digitalizado_idDocumentoLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function documentoDigitalizado()
    {
        return $this->belongsTo('App\Models\documento_digitalizado', 'documento_digitalizado_idDocumentoLng', 'idDocumentoLng');
    }
}

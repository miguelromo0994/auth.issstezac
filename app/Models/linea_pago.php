<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idLineaPagoLng
 * @property int $idPropuestaCobroLng
 * @property string $fechaLimitePagoDte
 * @property string $estatusLineaPagoStr
 * @property int $propuesta_cobro_idPropuestaCobroLng
 * @property PropuestaCobro $propuestaCobro
 */
class linea_pago extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'linea_pago';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idLineaPagoLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['idPropuestaCobroLng', 'fechaLimitePagoDte', 'estatusLineaPagoStr', 'propuesta_cobro_idPropuestaCobroLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function propuestaCobro()
    {
        return $this->belongsTo('App\Models\propuesta_cobro', 'propuesta_cobro_idPropuestaCobroLng', 'idPropuestaCobroLng');
    }
}

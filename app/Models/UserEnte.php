<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $idUsuario
 * @property int $idEnte
 */
class UserEnte extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'user_ente';

    /**
     * @var array
     */
    protected $fillable = ['idUsuario', 'idEnte'];
    public $timestamps = false;

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idNotaAutodeterminacionLng
 * @property string $notaAutodeterminacionStr
 * @property int $idRepresentanteLegalLng
 * @property string $firmaElectrÃ³nica
 * @property int $idMovimientoCobroLng
 * @property MovimientosCobro[] $movimientosCobros
 */
class nota_autodeterminacion extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'nota_autodeterminacion';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idNotaAutodeterminacionLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['notaAutodeterminacionStr', 'idRepresentanteLegalLng', 'firmaElectrÃ³nica', 'idMovimientoCobroLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function movimientosCobros()
    {
        return $this->hasMany('App\Models\movimientos_cobros', 'nota_autodeterminacion_idNotaAutodeterminacionLng', 'idNotaAutodeterminacionLng');
    }
}

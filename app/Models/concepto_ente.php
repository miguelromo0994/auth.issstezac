<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idConceptoEnteLng
 * @property int $idConceptoCobroLng
 * @property string $idEnteLng
 * @property string $tipoConcepto
 * @property string $conceptoEnteStr
 * @property string $descripcionConceptoStr
 * @property int $conceptoPermanenteLng
 * @property int $conceptoGrabableLng
 * @property int $esCuotaLng
 * @property int $cuentaLiquidez
 * @property int $cuentaPension
 * @property ConceptoCobro[] $conceptoCobros
 * @property EntePublico[] $entePublicos
 */
class concepto_ente extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'concepto_ente';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idConceptoEnteLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['idConceptoCobroLng', 'idEnteLng', 'tipoConcepto', 'conceptoEnteStr', 'descripcionConceptoStr', 'conceptoPermanenteLng', 'conceptoGrabableLng', 'esCuotaLng', 'cuentaLiquidez', 'cuentaPension'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function conceptoCobros()
    {
        return $this->hasMany('App\Models\concepto_cobro', 'concepto_ente_idConceptoEnteLng', 'idConceptoEnteLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entePublicos()
    {
        return $this->hasMany('App\Models\ente_publico', 'concepto_ente_idConceptoEnteLng', 'idConceptoEnteLng');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idRegimenIncorporacionInt
 * @property string $nombreRegimenIncorporacionStr
 * @property string $tipo_regimencol
 * @property EntePublico[] $entePublicos
 */
class tipo_regimen extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tipo_regimen';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idRegimenIncorporacionInt';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['nombreRegimenIncorporacionStr', 'tipo_regimencol'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entePublicos()
    {
        return $this->hasMany('App\Models\ente_publico', 'idRegimenIncorporacionInt', 'idRegimenIncorporacionInt');
    }
}

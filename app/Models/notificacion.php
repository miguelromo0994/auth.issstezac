<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idNotificacionLng
 * @property int $idTipoNotificacion
 * @property string $estatusNotificacionStr
 * @property string $fechaPresentacionDte
 * @property string $fechaEfectoDte
 * @property int $IdoportunidadPresentacionLng
 * @property int $idUsuarioPresentacionLng
 * @property int $idRepresentanteLegalLng
 * @property int $idCalendarioCobrosLng
 * @property int $notificacion_baja_idNotificacionBajaLng
 * @property int $notificacion_alta_idNotificacionAltaLng
 * @property int $idRelacionLaboralLng
 * @property int $idDerechohabienteLng
 * @property int $documento_digitalizado_idDocumentoLng
 * @property int $documento_digitalizado_idRelacionadoLng
 * @property int $documento_digitalizado_idTipoDocumentoLng
 * @property NotificacionAltum $notificacionAltum
 * @property NotificacionBaja $notificacionBaja
 * @property Derechohabiente[] $derechohabientes
 * @property NotificacionModificacion[] $notificacionModificacions
 * @property RelacionLaboral[] $relacionLaborals
 * @property RepresentanteLegal[] $representanteLegals
 */
class notificacion extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'notificacion';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idNotificacionLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['idTipoNotificacion', 'estatusNotificacionStr', 'fechaPresentacionDte', 'fechaEfectoDte', 'IdoportunidadPresentacionLng', 'idUsuarioPresentacionLng', 'idRepresentanteLegalLng', 'idCalendarioCobrosLng', 'notificacion_baja_idNotificacionBajaLng', 'notificacion_alta_idNotificacionAltaLng', 'idRelacionLaboralLng', 'idDerechohabienteLng', 'documento_digitalizado_idDocumentoLng', 'documento_digitalizado_idRelacionadoLng', 'documento_digitalizado_idTipoDocumentoLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notificacionAltum()
    {
        return $this->belongsTo('App\Models\notificacion_alta', 'notificacion_alta_idNotificacionAltaLng', 'idNotificacionAltaLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notificacionBaja()
    {
        return $this->belongsTo('App\Models\notificacion_baja', 'notificacion_baja_idNotificacionBajaLng', 'idNotificacionBajaLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function derechohabientes()
    {
        return $this->hasMany('App\Models\derechohabiente', 'notificacion_idNotificacionLng', 'idNotificacionLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notificacionModificacions()
    {
        return $this->hasMany('App\Models\notificacion_modificacion', 'notificacion_idNotificacionLng', 'idNotificacionLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function relacionLaborals()
    {
        return $this->hasMany('App\Models\relacion_laboral', 'notificacion_idNotificacionLng', 'idNotificacionLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function representanteLegals()
    {
        return $this->hasMany('App\Models\representante_legal', 'notificacion_idNotificacionLng', 'idNotificacionLng');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idTipoBajaLng
 * @property string $TipoBajaStr
 * @property int $periodo_cotizacion_idPeriodoCotizacionLng
 * @property int $notificacion_baja_idNotificacionBajaLng
 * @property NotificacionBaja $notificacionBaja
 * @property PeriodoCotizacion $periodoCotizacion
 */
class tipo_baja extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tipo_baja';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idTipoBajaLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['TipoBajaStr', 'periodo_cotizacion_idPeriodoCotizacionLng', 'notificacion_baja_idNotificacionBajaLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notificacionBaja()
    {
        return $this->belongsTo('App\Models\notificacion_baja', 'notificacion_baja_idNotificacionBajaLng', 'idNotificacionBajaLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function periodoCotizacion()
    {
        return $this->belongsTo('App\Models\periodo_cotizacion', 'periodo_cotizacion_idPeriodoCotizacionLng', 'idPeriodoCotizacionLng');
    }
}

<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idAsignacionRolesLng
 * @property int $idUsuarioLng
 * @property int $idRolLng
 * @property Role $role
 */
class asignacion_roles extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idAsignacionRolesLng';

    /**
     * @var array
     */
    protected $fillable = ['idUsuarioLng', 'idRolLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('App\Models\roles', 'idRolLng', 'idRolLng');
    }
}

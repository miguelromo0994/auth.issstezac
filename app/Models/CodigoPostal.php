<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $codigo
 * @property int $idMunicipio
 * @property Municipio $Municipio
 * @property MunicipiosAsentamiento[] $Asentamientos
 */
class CodigoPostal extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'municipios_codigopostal';

    /**
     * @var array
     */
    protected $fillable = ['codigo', 'idMunicipio'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Municipio()
    {
        return $this->belongsTo('App\Model\Municipio', 'idMunicipio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Asentamientos()
    {
        return $this->hasMany('App\Model\Asentamiento', 'idCodigoPostal');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idSindicatoLng
 * @property string $nombreSindicatoStr
 * @property string $descripcionStr
 * @property int $relacion_laboral_idRelacionLaboralLng
 */
class sindicato extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'sindicato';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idSindicatoLng';

    /**
     * @var array
     */
    protected $fillable = ['nombreSindicatoStr', 'descripcionStr', 'relacion_laboral_idRelacionLaboralLng'];

}

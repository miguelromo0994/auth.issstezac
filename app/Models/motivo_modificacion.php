<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idMotivoModificacion
 * @property string $motivoModificacionStr
 * @property int $notificacion_modificacion_idNotificacionModificacion
 * @property NotificacionModificacion $notificacionModificacion
 */
class motivo_modificacion extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'motivo_modificacion';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idMotivoModificacion';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['motivoModificacionStr', 'notificacion_modificacion_idNotificacionModificacion'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notificacionModificacion()
    {
        return $this->belongsTo('App\Models\notificacion_modificacion', 'notificacion_modificacion_idNotificacionModificacion', 'idNotificacionModificacion');
    }
}

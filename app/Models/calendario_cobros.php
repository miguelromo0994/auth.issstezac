<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idCalendarioCobrosLng
 * @property string $estatusCalendarioCobroStr
 * @property string $quincenaStr
 * @property string $fechaCorteDte
 * @property string $fechaEnvioDte
 * @property string $fechaQuincenaDte
 * @property string $fechaLimitePagoDte
 * @property string $fechaAutodeterminacionDte
 * @property string $fechaRecepcionPagoDte
 * @property string $fechaApicacionMovimientosDte
 * @property int $idEnteLng
 * @property int $propuesta_cobro_idPropuestaCobroLng
 * @property PropuestaCobro $propuestaCobro
 * @property EntePublico[] $entePublicos
 */
class calendario_cobros extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idCalendarioCobrosLng';

    /**
     * @var array
     */
    protected $fillable = ['estatusCalendarioCobroStr', 'quincenaStr', 'fechaCorteDte', 'fechaEnvioDte', 'fechaQuincenaDte', 'fechaLimitePagoDte', 'fechaAutodeterminacionDte', 'fechaRecepcionPagoDte', 'fechaApicacionMovimientosDte', 'idEnteLng', 'propuesta_cobro_idPropuestaCobroLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function propuestaCobro()
    {
        return $this->belongsTo('App\Models\propuesta_cobro', 'propuesta_cobro_idPropuestaCobroLng', 'idPropuestaCobroLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entePublicos()
    {
        return $this->hasMany('App\Models\ente_publico', 'calendario_cobros_idCalendarioCobrosLng', 'idCalendarioCobrosLng');
    }
}

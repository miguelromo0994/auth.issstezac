<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idAsignacionAmbitoLng
 * @property int $idUsuarioLng
 * @property int $idEnteLng
 * @property EntePublico $entePublico
 */
class asignacion_ambito extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'asignacion_ambito';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idAsignacionAmbitoLng';

    /**
     * @var array
     */
    protected $fillable = ['idUsuarioLng', 'idEnteLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entePublico()
    {
        return $this->belongsTo('App\Models\ente_publico', 'idEnteLng', 'idEnteLng');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idTabuladorLng
 * @property int $idEnteLng
 * @property int $relacion_laboral_idRelacionLaboralLng
 * @property int $notificacion_alta_idNotificacionAltaLng
 * @property int $notificacion_modificacion_idNotificacionModificacion
 * @property NotificacionAltum $notificacionAltum
 * @property NotificacionModificacion $notificacionModificacion
 * @property RelacionLaboral $relacionLaboral
 * @property EntePublico[] $entePublicos
 */
class tabulador extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tabulador';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idTabuladorLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['idEnteLng', 'relacion_laboral_idRelacionLaboralLng', 'notificacion_alta_idNotificacionAltaLng', 'notificacion_modificacion_idNotificacionModificacion'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notificacionAltum()
    {
        return $this->belongsTo('App\Models\notificacion_alta', 'notificacion_alta_idNotificacionAltaLng', 'idNotificacionAltaLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notificacionModificacion()
    {
        return $this->belongsTo('App\Models\notificacion_modificacion', 'notificacion_modificacion_idNotificacionModificacion', 'idNotificacionModificacion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function relacionLaboral()
    {
        return $this->belongsTo('App\Models\relacion_laboral', 'relacion_laboral_idRelacionLaboralLng', 'idRelacionLaboralLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entePublicos()
    {
        return $this->hasMany('App\Models\ente_publico', 'tabulador_idTabuladorLng', 'idTabuladorLng');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idBeneficiarioLng
 * @property string $estatusStr
 * @property string $rfcStr
 * @property string $curpStr
 * @property string $nombreStr
 * @property string $primerApellidoStr
 * @property string $segundoApellidoStr
 * @property string $fechaNacimientoDte
 * @property string $sexoStr
 * @property string $TelefonoStr
 * @property int $esDerechohabienteBool
 * @property string $observacionesStr
 * @property int $idDerechohabienteLng
 * @property int $idParentescoLng
 * @property int $idEstadoCivilLng
 * @property int $idMunicipioLng
 * @property int $idGrupoSanguineoLng
 * @property integer $domicilio_idDomicilioLng
 * @property int $municipio_idMunicipioLng
 * @property int $estado_civil_idEstadoCivilLng
 * @property int $parentezco_idParentescoLng
 * @property int $documento_digitalizado_idDocumentoLng
 * @property int $documento_digitalizado_idRelacionadoLng
 * @property int $documento_digitalizado_idTipoDocumentoLng
 * @property Domicilio $domicilio
 * @property EstadoCivil $estadoCivil
 * @property Municipio $municipio
 * @property Parentesco $parentesco
 * @property Derechohabiente[] $derechohabientes
 * @property GrupoSanguineo $grupoSanguineo
 */
class beneficiarios extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idBeneficiarioLng';

    /**
     * @var array
     */
    protected $fillable = ['estatusStr', 'rfcStr', 'curpStr', 'nombreStr', 'primerApellidoStr', 'segundoApellidoStr', 'fechaNacimientoDte', 'sexoStr', 'TelefonoStr', 'esDerechohabienteBool', 'observacionesStr', 'idDerechohabienteLng', 'idParentescoLng', 'idEstadoCivilLng', 'idMunicipioLng', 'idGrupoSanguineoLng', 'domicilio_idDomicilioLng', 'municipio_idMunicipioLng', 'estado_civil_idEstadoCivilLng', 'parentezco_idParentescoLng', 'documento_digitalizado_idDocumentoLng', 'documento_digitalizado_idRelacionadoLng', 'documento_digitalizado_idTipoDocumentoLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function domicilio()
    {
        return $this->belongsTo('App\Models\domicilio', 'domicilio_idDomicilioLng', 'idDomicilioLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estadoCivil()
    {
        return $this->belongsTo('App\Models\estado_civil', 'estado_civil_idEstadoCivilLng', 'idEstadoCivilLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function municipio()
    {
        return $this->belongsTo('App\Models\municipio', 'municipio_idMunicipioLng', 'idMunicipioLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentesco()
    {
        return $this->belongsTo('App\Models\parentesco', 'parentezco_idParentescoLng', 'idParentescoLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function derechohabientes()
    {
        return $this->hasMany('App\Models\derechohabiente', 'beneficiarios_idBeneficiarioLng', 'idBeneficiarioLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function grupoSanguineo()
    {
        return $this->hasOne('App\Models\grupo_sanguineo', 'idGrupoSanguineoLng', 'idBeneficiarioLng');
    }
}

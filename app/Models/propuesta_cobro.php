<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idPropuestaCobroLng
 * @property int $idCalendarioCobrosLng
 * @property string $firma_digital
 * @property int $movimientos_cobros_idMovimientoCobroLng
 * @property MovimientosCobro $movimientosCobro
 * @property CalendarioCobro[] $calendarioCobros
 * @property LineaPago[] $lineaPagos
 */
class propuesta_cobro extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'propuesta_cobro';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idPropuestaCobroLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['idCalendarioCobrosLng', 'firma_digital', 'movimientos_cobros_idMovimientoCobroLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function movimientosCobro()
    {
        return $this->belongsTo('App\Models\movimientos_cobros', 'movimientos_cobros_idMovimientoCobroLng', 'idMovimientoCobroLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function calendarioCobros()
    {
        return $this->hasMany('App\Models\calendario_cobro', 'propuesta_cobro_idPropuestaCobroLng', 'idPropuestaCobroLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lineaPagos()
    {
        return $this->hasMany('App\Models\linea_pago', 'propuesta_cobro_idPropuestaCobroLng', 'idPropuestaCobroLng');
    }
}

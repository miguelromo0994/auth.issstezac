<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idRolLng
 * @property string $nombreRolStr
 * @property string $descripcionStr
 * @property int $siguiente_nivel_con_normatividad
 * @property string $estatus_autorizacion_con_normatividad
 * @property int $siguiente_nivel_no_cumple_normatividad
 * @property string $estatus_no_cumple_normatividad
 * @property AsignacionRole[] $asignacionRoles
 */
class roles extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idRolLng';

    /**
     * @var array
     */
    protected $fillable = ['nombreRolStr', 'descripcionStr', 'siguiente_nivel_con_normatividad', 'estatus_autorizacion_con_normatividad', 'siguiente_nivel_no_cumple_normatividad', 'estatus_no_cumple_normatividad'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function asignacionRoles()
    {
        return $this->hasMany('App\Models\asignacion_roles', 'idRolLng', 'idRolLng');
    }
}

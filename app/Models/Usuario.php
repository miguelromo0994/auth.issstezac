<?php

namespace App\Models;
use Illuminate\Support\Facades\Hash;

use Illuminate\Database\Eloquent\Model;

/**

 */
class Usuario extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'users';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'nombre',
        'primerApellido',
        'segundoApellido',
        'iniciales',
        'idRol',
    ];

    public function setPasswordAttribute($value)
    {
         $this->attributes['password'] = Hash::make($value);
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $estatus
 * @property string $rfc
 * @property string $curp
 * @property string $nombre
 * @property string $primerApellido
 * @property string $segundoApellido
 * @property string $fechaNac
 * @property string $sexo
 * @property string $telefono
 * @property int $esDerechohabiente
 * @property string $observaciones
 * @property int $idGrupoSanguineo
 * @property int $idDerechohabiente
 * @property int $idParentesco
 * @property int $idEstadoCivil
 * @property int $idDomicilio
 */
class Beneficiario extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'beneficiario';

    /**
     * @var array
     */
    protected $fillable = [ 'estatus',
                            'rfc', 
                            'curp', 
                            'nombre', 
                            'primerApellido', 
                            'segundoApellido', 
                            'fechaNac', 
                            'sexo',
                            'telefono', 
                            'esDerechohabiente', 
                            'observaciones', 
                            'idGrupoSanguineo', 
                            'idDerechohabiente', 
                            'idParentesco', 
                            'idEstadoCivil', 
                            'idDomicilio'
                    ];

    public $timestamps = false;


}

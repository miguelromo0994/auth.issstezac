<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $idCuotaAportacionLng
 * @property string $quincena
 * @property float $montoCuotaDbl
 * @property float $montoAportacionDbl
 * @property float $porcentajeCuotaLng
 * @property float $sueldoCotizacionDbl
 * @property string $fechaPagoDte
 * @property float $porcentajeAportacion
 * @property string $formaPagoInt
 * @property int $conceptoInt
 * @property int $devueltaInt
 * @property int $idRelacionLaboralLng
 * @property int $idDerechohabienteLng
 * @property int $idGeneracionLng
 * @property int $relacion_laboral_idRelacionLaboralLng
 * @property RelacionLaboral $relacionLaboral
 * @property Derechohabiente $derechohabiente
 */
class cuota_aportacion extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'cuota_aportacion';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idCuotaAportacionLng';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['quincena', 'montoCuotaDbl', 'montoAportacionDbl', 'porcentajeCuotaLng', 'sueldoCotizacionDbl', 'fechaPagoDte', 'porcentajeAportacion', 'formaPagoInt', 'conceptoInt', 'devueltaInt', 'idRelacionLaboralLng', 'idDerechohabienteLng', 'idGeneracionLng', 'relacion_laboral_idRelacionLaboralLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function relacionLaboral()
    {
        return $this->belongsTo('App\Models\relacion_laboral', 'relacion_laboral_idRelacionLaboralLng', 'idRelacionLaboralLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function derechohabiente()
    {
        return $this->hasOne('App\Models\derechohabiente', 'idDerechohabienteLng', 'idDerechohabienteLng');
    }
}

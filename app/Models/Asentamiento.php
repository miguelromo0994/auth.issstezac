<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombre
 * @property string $tipo
 * @property string $ciudad
 * @property string $zona
 * @property string $idAsenta
 * @property int $idCodigoPostal
 * @property CodigoPostal $CodigoPostal
 */
class Asentamiento extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'municipios_asentamiento';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'tipo', 'ciudad', 'zona', 'idAsenta', 'idCodigoPostal'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Codigopostal()
    {
        return $this->belongsTo('App\Models\Codigopostal', 'idCodigoPostal');
    }
}

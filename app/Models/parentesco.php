<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idParentescoLng
 * @property string $nombreParentescoStr
 * @property int $documento_digitalizado_idDocumentoLng
 * @property int $documento_digitalizado_idRelacionadoLng
 * @property int $documento_digitalizado_idTipoDocumentoLng
 * @property int $documento_digitalizado_nota_auto_idNotaAutoLng
 * @property Beneficiario[] $beneficiarios
 */
class parentesco extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'parentesco';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idParentescoLng';

    /**
     * @var array
     */
    protected $fillable = ['nombreParentescoStr', 'documento_digitalizado_idDocumentoLng', 'documento_digitalizado_idRelacionadoLng', 'documento_digitalizado_idTipoDocumentoLng', 'documento_digitalizado_nota_auto_idNotaAutoLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function beneficiarios()
    {
        return $this->hasMany('App\Models\beneficiario', 'parentezco_idParentescoLng', 'idParentescoLng');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idMensajeLng
 * @property int $idRemitenteLng
 * @property int $idTipoMensaje
 * @property string $estatusMensajeStr
 * @property string $tituloMensajeStr
 * @property string $fechaMensajeDte
 * @property string $mesajeStr
 * @property string $adjuntos
 * @property int $destinatario_mensaje_idDestinatarioMensajeLng
 * @property int $documento_digitalizado_idDocumentoLng
 * @property int $documento_digitalizado_idRelacionadoLng
 * @property int $documento_digitalizado_idTipoDocumentoLng
 * @property int $documento_digitalizado_nota_auto_idNotaAutoLng
 * @property DestinatarioMensaje $destinatarioMensaje
 */
class mensaje extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'mensaje';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idMensajeLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['idRemitenteLng', 'idTipoMensaje', 'estatusMensajeStr', 'tituloMensajeStr', 'fechaMensajeDte', 'mesajeStr', 'adjuntos', 'destinatario_mensaje_idDestinatarioMensajeLng', 'documento_digitalizado_idDocumentoLng', 'documento_digitalizado_idRelacionadoLng', 'documento_digitalizado_idTipoDocumentoLng', 'documento_digitalizado_nota_auto_idNotaAutoLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function destinatarioMensaje()
    {
        return $this->belongsTo('App\Models\destinatario_mensaje', 'destinatario_mensaje_idDestinatarioMensajeLng', 'idDestinatarioMensajeLng');
    }
}

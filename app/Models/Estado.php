<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombre
 * @property int $codigo
 * @property Municipios[] $Municipios
 */
class Estado extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'municipios_estado';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'codigo'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Municipios()
    {
        return $this->hasMany('App\Models\Municipio', 'idEstado');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $parentesco
 */
class BeneficiarioParentesco extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'beneficiario_parentesco';

    /**
     * @var array
     */
    protected $fillable = ['parentesco'];

    public $timestamps = false;

}

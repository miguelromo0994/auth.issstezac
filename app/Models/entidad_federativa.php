<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idEntidadFederativaLng
 * @property string $nombreEntidadStr
 * @property string $abreviaturaStr
 * @property Municipio[] $municipios
 */
class entidad_federativa extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'entidad_federativa';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idEntidadFederativaLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['nombreEntidadStr', 'abreviaturaStr'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function municipios()
    {
        return $this->hasMany('App\Models\municipio', 'idEntidadFederativaLng', 'idEntidadFederativaLng');
    }
}

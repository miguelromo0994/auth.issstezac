<?php

namespace App\Http\Controllers;

Use App\Models\DerechohabienteTipo;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\DerechohabienteTipoTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreDerechohabienteTipoRequest;



class DerechohabienteTipoController extends Controller
{ 

    use Helpers;


    /**
     * Show all Tipo de derechohabiente.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos = DerechohabienteTipo::all()->sortby('descripcion');
        
        return $this->collection($tipos, new DerechohabienteTipoTransformer);
    }


    /**
     * Show data of the specified tipo de derechohabiente.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $tipo = DerechohabienteTipo::where('id', $id)->first();

        if ($tipo) {
            return $this->item($tipo, new DerechohabienteTipoTransformer);
        }

        return $this->response->errorNotFound();
    }


    /**
     * Creata a new tipo de derechohabiente.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreDerechohabienteTipoRequest $request)
    {
         $id = DerechohabienteTipo::insertGetId($request->all());
         if ($id) {
                 return $id;
         }


        return $this->response->errorBadRequest();
    }
    

    /**
     * Update the tipo de derechohabiente.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Requests\StoreDerechohabienteTipoRequest $request, $id)
    {

            $tipo = DerechohabienteTipo::findOrFail($id);
    
            $tipo->update($request->all());

    
        if ($tipo) {
            return $this->item($tipo, new DerechohabienteTipoTransformer);
        }
        
        return $this->response->errorBadRequest();
    }

    /**
     * Remove the specified tipo de derechohabiente.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipo = DerechohabienteTipo::find($id);

        if ($tipo) {
            $tipo->delete();
            return $this->response->noContent();
        }

        return $this->response->errorBadRequest();
    }




}
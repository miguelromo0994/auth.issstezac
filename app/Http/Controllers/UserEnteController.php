<?php

namespace App\Http\Controllers;

Use App\Models\UserEnte;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\UserEnteTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreUserEnteRequest;



class UserEnteController extends Controller
{ 

    use Helpers;


    /**
     * Show all User-Entes.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userEntes = UserEnte::all();
        
        return $this->collection($userEntes, new UserEnteTransformer);
    }


    /**
     * Show data of the specified User Ente.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $userEnte = UserEnte::where('id', $id)->first();

        if ($userEnte) {
            return $this->item($userEnte, new UserEnteTransformer);
        }

        return $this->response->errorNotFound();
    }


    /**
     * Creata a new User Ente.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreUserEnteRequest $request)
    {

         $id = UserEnte::insertGetId($request->all());
         if ($id) {
                 return $id;
            }


        return $this->response->errorBadRequest();
    }

    /**
     * Update the User-Ente.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Requests\StoreUserEnteRequest $request, $id)
    {

            $userEnte = UserEnte::findOrFail($id);
    
            $userEnte->update($request->all());

    
        if ($userEnte) {
            return $this->item($userEnte, new UserEnteTransformer);
        }
        
        return $this->response->errorBadRequest();
    }

    /**
     * Remove the specified User-Ente.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userEnte = UserEnte::find($id);

        if ($userEnte) {
            $userEnte->delete();
            return $this->response->noContent();
        }

        return $this->response->errorBadRequest();
    }


}
<?php

namespace App\Http\Controllers;

Use App\Models\Vialidad;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\VialidadTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreVialidadRequest;



class VialidadController extends Controller
{ 

    use Helpers;


    /**
     * Mostrar todas las vialidades
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vialidades = Vialidad::all();
        
        return $this->collection($vialidades, new VialidadTransformer);
    }


    /**
     * Mostrar datos de una vialidad
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $vialidad = Vialidad::where('id', $id)->first();

        if ($vialidad) {
            return $this->item($vialidad, new VialidadTransformer);
        }

        return $this->response->errorNotFound();
    }


    /**
     * Crear nueva vialidad
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreVialidadRequest $request)
    {

         $id = Vialidad::insertGetId($request->all());
        
            if ($id) {
                 return $id;
            }

        return $this->response->errorBadRequest();
    }

    /**
     * Update the grupo sanguineo.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Requests\StoreVialidadRequest $request, $id)
    {

            $vialidad = Vialidad::findOrFail($id);
    
            $vialidad->update($request->all());

    
        if ($vialidad) {
            return $this->item($vialidad, new VialidadTransformer);
        }
        
        return $this->response->errorBadRequest();
    }

    /**
     * Remove the specified grupo sanguineo.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vialidad = Vialidad::find($id);

        if ($vialidad) {
            $vialidad->delete();
            return $this->response->noContent();
        }

        return $this->response->errorBadRequest();
    }




}
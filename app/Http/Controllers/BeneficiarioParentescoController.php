<?php

namespace App\Http\Controllers;

Use App\Models\BeneficiarioParentesco;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\BeneficiarioParentescoTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreBeneficiarioParentescoRequest;



class BeneficiarioParentescoController extends Controller
{ 

    use Helpers;


    /**
     * Mostrar todos los parentescos.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parentescos = BeneficiarioParentesco::all();
        
        return $this->collection($parentescos, new BeneficiarioParentescoTransformer);
    }


    /**
     * Mostrar los datos de un parentesco.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $parentesco = BeneficiarioParentesco::where('id', $id)->first();

        if ($parentesco) {
            return $this->item($parentesco, new GrupoSanguineoTransformer);
        }

        return $this->response->errorNotFound();
    }


    /**
     * Crear un nuevo parentesco.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreBeneficiarioParentescoRequest $request)
    {

         $id = BeneficiarioParentesco::insertGetId($request->all());
        
            if ($id) {
                 return $id;
            }

        return $this->response->errorBadRequest();
    }

    /**
     * Actualizar un parentesco.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Requests\StoreBeneficiarioParentescoRequest $request, $id)
    {

            $beneficiario = BeneficiarioParentesco::findOrFail($id);
    
            $beneficiario->update($request->all());

    
        if ($beneficiario) {
            return $this->item($beneficiario, new BeneficiarioParentescoTransformer);
        }
        
        return $this->response->errorBadRequest();
    }

    /**
     * Eliminar un parentesco.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $parentesco = BeneficiarioParentesco::find($id);

        if ($parentesco) {
            $parentesco->delete();
            return $this->response->noContent();
        }

        return $this->response->errorBadRequest();
    }




}
<?php

namespace App\Http\Controllers;

Use App\Models\EntePublico;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\EnteTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreEnteRequest;



class EnteController extends Controller
{ 

    use Helpers;


    /**
     * Show all Users.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entes = EntePublico::all();
        
        return $this->collection($entes, new EnteTransformer);
    }


    /**
     * Show data of the specified User.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $ente = EntePublico::where('id', $id)->first();

        if ($ente) {
            return $this->item($ente, new EnteTransformer);
        }

        return $this->response->errorNotFound();
    }


    /**
     * Creata a new Ente.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreEnteRequest $request)
    {

        if (EntePublico::Create($request->all())) {
                return $this->response->created();
            }

        return $this->response->errorBadRequest();
    }

    /**
     * Update the Ente.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Requests\StoreEnteRequest $request, $id)
    {

            $ente = EntePublico::findOrFail($id);
    
            $ente->update($request->all());

    
        if ($ente) {
            return $this->item($ente, new EnteTransformer);
        }
        
        return $this->response->errorBadRequest();
    }

    /**
     * Remove the specified Ente.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ente = EntePublico::find($id);

        if ($ente) {
            $ente->delete();
            return $this->response->noContent();
        }

        return $this->response->errorBadRequest();
    }


    /**
     * Return the Entes list by User id.
     *
     * @param  int $idUser
     *
     * @return \Illuminate\Http\Response
     */
    public function enteByUser($idUser)
    {

           $entes = EntePublico::select('ente_publico.id as idEnte',
                'ente_publico.nombre as nombreEnte', 'user_ente.id as idUserEnte')
                ->leftjoin('user_ente', 'ente_publico.id', '=', 'user_ente.id')
                ->where('user_ente.idUsuario',$idUser)
                ->get();
        
       return $this->response->array(['data' => $entes], 200);
    }



}
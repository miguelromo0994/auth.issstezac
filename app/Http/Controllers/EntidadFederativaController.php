<?php

namespace App\Http\Controllers;

Use App\Models\EntidadFederativa;
Use App\Models\Municipio;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\EntidadFederativaTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreEntidadFederativaRequest;



class EntidadFederativaController extends Controller
{ 

    use Helpers;


    /**
     * Show all entidades federativas.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entidades = EntidadFederativa::all()->sortby('nombre');
        
        return $this->collection($entidades, new EntidadFederativaTransformer);
    }


    /**
     * Show data of the specified Entidad Federativa.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $entidad = EntidadFederativa::where('id', $id)->first();

        if ($entidad) {
            return $this->item($entidad, new EntidadFederativaTransformer);
        }

        return $this->response->errorNotFound();
    }


    /**
     * Creata a new Entidad Federativa.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreEntidadFederativaRequest $request)
    {
        $id = EntidadFederativa::insertGetId($request->all());
        if ($id) {
                 return $id;
            }

        return $this->response->errorBadRequest();
    }

    /**
     * Update the Entidad Federativa.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Requests\StoreEntidadFederativaRequest $request, $id)
    {

            $entidad = EntidadFederativa::findOrFail($id);
    
            $entidad->update($request->all());

    
        if ($entidad) {
            return $this->item($entidad, new EntidadFederativaTransformer);
        }
        
        return $this->response->errorBadRequest();
    }

    /**
     * Remove the specified Entidad Federativa.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entidad = EntidadFederativa::find($id);

        if ($entidad) {
            $entidad->delete();
            return $this->response->noContent();
        }

        return $this->response->errorBadRequest();
    }



  /**
     * Return the Municipios list by Entidad Federativa id.
     *
     * @param  int $idEntidad
     *
     * @return \Illuminate\Http\Response
     */
    public function municipios($idEntidad)
    {

           $municipios = Municipio::select('municipio.id as idMunicipio',
                'municipio.nombre as nombreMunicipio','municipio.cveMun',
                'municipio.idEntidadFederativa')
                ->where('municipio.idEntidadFederativa',$idEntidad)
                ->orderby('municipio.nombre')
                ->get();
           
        
       return $this->response->array(['data' => $municipios], 200);
    }


}
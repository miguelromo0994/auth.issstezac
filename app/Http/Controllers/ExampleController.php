<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Example;
use App\Models\User;

use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

// use Illuminate\Support\Facades\Auth;

class ExampleController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:create example'], ['only' => ['create', 'store']]);
        $this->middleware(['permission:read example'], ['only' => ['index','show']]);
        $this->middleware(['permission:update example'], ['only' => ['edit', 'update']]);
        $this->middleware(['permission:delete example'], ['only' => 'delete']);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rol = Auth::user()->roles->implode('name',',');
        switch ($rol){
            case ('super-admin'):
                $saludo = 'Bienvenido super-admin';

                return view('example.index', compact('saludo'));
            break;
            case ('moderador'):
                $saludo = 'Bienvenido moderador';

                return view('example.index', compact('saludo'));
            break;
            case ('editor'):
                $saludo = 'Bienvenido editor';

                return view('example.index', compact('saludo'));
            break;
        } 
        return view('example.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('example.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Example  $example
     * @return \Illuminate\Http\Response
     */
    public function show(Example $example)
    {
        return view('example.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Example  $example
     * @return \Illuminate\Http\Response
     */
    public function edit(Example $example)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Example  $example
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Example $example)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Example  $example
     * @return \Illuminate\Http\Response
     */
    public function destroy(Example $example)
    {
        //
    }
}

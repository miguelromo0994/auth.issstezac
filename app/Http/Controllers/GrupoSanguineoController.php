<?php

namespace App\Http\Controllers;

Use App\Models\GrupoSanguineo;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\GrupoSanguineoTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreGrupoSanguineoRequest;



class GrupoSanguineoController extends Controller
{ 

    use Helpers;


    /**
     * Show all Grupos Sanguineos.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grupos = GrupoSanguineo::all();
        
        return $this->collection($grupos, new GrupoSanguineoTransformer);
    }


    /**
     * Show data of the specified Grupo sanguineo.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $grupo = GrupoSanguineo::where('id', $id)->first();

        if ($grupo) {
            return $this->item($grupo, new GrupoSanguineoTransformer);
        }

        return $this->response->errorNotFound();
    }


    /**
     * Creata a new grupo sanguineo.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreGrupoSanguineoRequest $request)
    {

         $id = GrupoSanguineo::insertGetId($request->all());
        
            if ($id) {
                 return $id;
            }

        return $this->response->errorBadRequest();
    }

    /**
     * Update the grupo sanguineo.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Requests\StoreGrupoSanguineoRequest $request, $id)
    {

            $grupo = GrupoSanguineo::findOrFail($id);
    
            $grupo->update($request->all());

    
        if ($grupo) {
            return $this->item($grupo, new GrupoSanguineoTransformer);
        }
        
        return $this->response->errorBadRequest();
    }

    /**
     * Remove the specified grupo sanguineo.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grupo = GrupoSanguineo::find($id);

        if ($grupo) {
            $grupo->delete();
            return $this->response->noContent();
        }

        return $this->response->errorBadRequest();
    }




}
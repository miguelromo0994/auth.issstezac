<?php

namespace App\Http\Controllers;

Use App\Models\Estado;
Use App\Models\Municipio;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\EstadoTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreEstadoRequest;



class EstadoController extends Controller
{ 

    use Helpers;


    /**
     * Show all entidades federativas.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estado = Estado::all()->sortby('nombre');
        
        return $this->collection($estado, new EstadoTransformer);
    }


    /**
     * Show data of the specified Entidad Federativa.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $estado = Estado::where('id', $id)->first();

        if ($estado) {
            return $this->item($estado, new EstadoTransformer);
        }

        return $this->response->errorNotFound();
    }






    /**
     * Regresa la lista de Municipios por id de Estado.
     *
     * @param  int $idEntidad
     *
     * @return \Illuminate\Http\Response
     */
    public function municipios($id)
    {

           $municipios = Municipio::select('municipios_municipio.*')
                ->where('municipios_municipio.idEstado',$id)
                ->orderby('municipios_municipio.nombre')
                ->get();
           
        
       return $this->response->array(['data' => $municipios], 200);
    }



}
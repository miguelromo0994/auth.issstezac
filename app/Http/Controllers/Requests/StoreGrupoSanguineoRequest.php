<?php

namespace App\Http\Controllers\Requests;

use Dingo\Api\Http\FormRequest;

class StoreGrupoSanguineoRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'grupoSanguineo' => 'required|unique:grupo_sanguineo,grupoSanguineo,'.$this->id,
                
    
        ];
    }


    public function messages()
    {
        return [
                'grupoSanguineo.required'=> 'Grupo Sanguíneo requerido',
                
        ];

    }

}
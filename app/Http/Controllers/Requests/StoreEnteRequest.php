<?php

namespace App\Http\Controllers\Requests;

use Dingo\Api\Http\FormRequest;

class StoreEnteRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'nombre'    => 'required|string|max:255', 
                'rfcPatronal' => 'required|string|max:20', 
                'telefono'    => 'required|string|max:20', 
                'clasificacion' => 'required', 
                'fechaIncorporacion' => 'required', 
                
                'porcentajeCuota'       => 'required', 
                'porcentajeAportacion'  => 'required', 
                'claveRegistroPatronal' => 'required', 
                'idRegimenIncorporacion' => 'required', 
                'idEnteEstatus'         => 'required', 
                'idRepresentanteLegal'  => 'required',
                'idTabulador'       => 'required',
                'idDomicilio'       => 'required',
                'idCalendarioCobros'=> 'required',
                'idEnteConcepto'    => 'required',
        ];
    }


    public function messages()
    {
        return [
                'nombre.required'    => 'Nombre del ente público requerido', 
                'rfcPatronal.required' => 'RFC Patronal requerido', 
                'telefono.required'    => 'Teléfono requerido', 
                'clasificacion.required' => 'Clasificación requerida', 
                'fechaIncorporacion.required' => 'Fecha de incorporación requerida', 
               
                'porcentajeCuota.required'       => 'Porcentaje de cuota requerido', 
                'porcentajeAportacion.required'  => 'Porcentaje de aportación requerida', 
                'claveRegistroPatronal.required' => 'Clave de registro patronal requerida', 
                'idRegimenIncorporacion.required' => 'Regimen de incorporación requerido', 
                'idEnteEstatus.required'         => 'Estatus del Ente público requerido', 
                'idRepresentanteLegal.required'  => 'Representante legal requerido',
                'idTabulador.required'       => 'Tabulador requerido',
                'idDomicilio.required'       => 'Domicilio requerido',
                'idCalendarioCobros.required'=> 'Calendario de cobros requerido',
                'idEnteConcepto.required'    => 'Concepto del ente público requerido',

        ];

    }

}
<?php

namespace App\Http\Controllers\Requests;

use Dingo\Api\Http\FormRequest;

class StoreUserEnteRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'idUsuario' => 'required',
                'idEnte' => 'required',
    
        ];
    }


    public function messages()
    {
        return [
                'idUsuario.required'=> 'Usuario requerido',
                'idEnte.required' => 'Ente requerido',
        ];

    }

}
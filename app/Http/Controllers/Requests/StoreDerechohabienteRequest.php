<?php

namespace App\Http\Controllers\Requests;

use Dingo\Api\Http\FormRequest;

class StoreDerechohabienteRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'estatus'   => 'required', 
                'rfc' => 'required|unique:derechohabiente,rfc,'.$this->id,
                'curp' => 'required|unique:derechohabiente,curp,'.$this->id,
                'nss' => 'required|unique:derechohabiente,nss,'.$this->id,
                'nombre'    => 'required', 
                'primerApellido'  => 'required', 
                'segundoApellido' => 'required', 
                'sexo'      => 'required', 
                'fechaNac'      => 'required', 
                'fechaIngreso'  => 'required', 
                'estatusBloqueo' => 'required', 
                'telefono'      => 'required', 
                'celular'       => 'required', 
                'email'         => 'required', 
                'emailAlterno'  => 'required', 
                'numeroIssstezac' => 'required', 
                'idEstadoCivil' => 'required',
                'idDerechohabienteTipo' => 'required', 
                'idGrupoSanguineo'  => 'required',     
        ];
    }


    public function messages()
    {
        return [
                'estatus.required'   => 'Estatus requerido', 
                'rfc.required'       => 'RFC requerido', 
                'curp.required'      => 'CURP requerida', 
                'nss.required'       => 'NSS requerido', 
                'nombre.required'    => 'Nombre requerido', 
                'primerApellido.required'  => 'Primer apellido requerido', 
                'segundoApellido.required' => 'Segundo apellido requerido', 
                'sexo.required'      => 'Sexo requerido', 
                'fechaNac.required'      => 'Fecha de nacimiento requerida', 
                'fechaIngreso.required'  => 'Fecha de ingreso requerida', 
                'estatusBloqueo.required' => 'Estatus de bloqueo requerido', 
                'telefono.required'      => 'Teléfono requerido', 
                'celular.required'       => 'Celular requerido', 
                'email.required'         => 'Email requerido', 
                'emailAlterno.required'  => 'Email alterno requerido', 
                'numeroIssstezac.required' => 'Número Issstezac requerido', 
                'idEstadoCivil.required' => 'Estado civil requerido',
                'idDerechohabienteTipo.required' => 'Tipo de derechohabiente requerido', 
                'idGrupoSanguineo.required'  => 'Grupo sanguíneo requerido',  
        ];

    }

}
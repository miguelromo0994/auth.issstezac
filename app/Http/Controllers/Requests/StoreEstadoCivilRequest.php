<?php

namespace App\Http\Controllers\Requests;

use Dingo\Api\Http\FormRequest;

class StoreEstadoCivilRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'nombreEstado' => 'required|unique:estado_civil,nombreEstado,'.$this->id,
                
    
        ];
    }


    public function messages()
    {
        return [
                'nombreEstado.required'=> 'Nombre del estado civil requerido',
                
        ];

    }

}
<?php

namespace App\Http\Controllers\Requests;

use Dingo\Api\Http\FormRequest;

class StoreEntidadFederativaRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'nombre' => 'required|unique:entidad_federativa,nombre,'.$this->id,
                'cveEnt' => 'required|unique:entidad_federativa,cveEnt,'.$this->id,
                'abreviatura' => 'required|unique:entidad_federativa,abreviatura,'.$this->id,
              
                
    
        ];
    }


    public function messages()
    {
        return [
                'nombre.required'=> 'Nombre de la entidad federativa requerida',
                'nombre.unique'=> 'El nombre de la entidad federativa ya existe',
                'cveEnt.required'=> 'Clave de la entidad federativa requerida',
                'cveEnt.unique'=> 'Clave de la entidad federativa ya existe', 
                'abreviatura.required'=> 'Abreviatura de la entidad federativa requerida',
                'abreviatura.unique'=> 'Abreviatura de la entidad federativa ya existe',
                
                
        ];

    }

}
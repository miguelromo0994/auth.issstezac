<?php

namespace App\Http\Controllers\Requests;

use Dingo\Api\Http\FormRequest;

class StoreDomicilioRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'idVialidad'    => 'required', 
                'nombreVialidad'=> 'required',
                'numero'        => 'required', 
                'idAsentamiento'=> 'required', 
                'idTipoDomicilio' => 'required',               

        ];
    }


    public function messages()
    {
        return [
                'idVialidad.required' => 'Tipo de vialidad requerida',
                'nombreVialidad.required' => 'Nombre de la vialidad requerida',
                'numero.required' => 'Número exterior e interior requerido',
                'idAsentamiento.required' => 'Asentamiento requerido',
                'idTipoDomicilio.required' => 'Tipo de domicilio requerido',
                
        ];

    }

}
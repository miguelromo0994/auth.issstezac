<?php

namespace App\Http\Controllers\Requests;

use Dingo\Api\Http\FormRequest;

class StoreUserRolRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'nombreRol' => 'required|unique:user_rol,nombreRol,'.$this->id,
                'descripcion' => 'required|string|max:255',
    
        ];
    }


    public function messages()
    {
        return [
                'nombreRol.required'=> 'Nombre de rol requerido',
                'descripcion.required' => 'Descripción del rol requerida',
        ];

    }

}